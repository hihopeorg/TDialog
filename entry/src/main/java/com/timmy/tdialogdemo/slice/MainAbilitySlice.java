package com.timmy.tdialogdemo.slice;

import com.timmy.tdialog.TDialog;
import com.timmy.tdialog.base.BindViewHolder;
import com.timmy.tdialog.base.TBaseAdapter;
import com.timmy.tdialog.list.TListDialog;
import com.timmy.tdialog.listener.OnBindViewListener;
import com.timmy.tdialog.listener.OnViewClickListener;
import com.timmy.tdialogdemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.multimodalinput.event.KeyEvent;

import java.lang.reflect.Method;
import java.util.Arrays;

import static ohos.agp.colors.RgbColor.fromArgbInt;

public class MainAbilitySlice extends AbilitySlice {
    private TDialog tDialog;
    private MyEventHandler handler;
    private static final String TAG = "TDialog";
    private static final int WHAT_LOADING = 0;
    private static final int WHAT_PROGRESS = 1;
    private String[] data = {"java", "ohos", "NDK", "c++", "python", "ios", "Go", "Unity3D", "Kotlin", "Swift", "js"};
    private String[] sharePlatform = {"微信", "朋友圈", "短信", "微博", "QQ空间", "Google", "FaceBook", "微信", "朋友圈", "短信", "微博", "QQ空间"};
    int currProgress = 5;
    private ProgressBar progressBar;
    private Text tvProgress;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        handler = new MyEventHandler(EventRunner.getMainEventRunner());
        Window window = getWindow(); window.setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
        findComponentById(ResourceTable.Id_button2).setClickedListener(component -> {
            useTDialog();
        });
        findComponentById(ResourceTable.Id_button3).setClickedListener(component -> {
            upgradeDialog();
        });
        findComponentById(ResourceTable.Id_button4).setClickedListener(component -> {
            upgradeDialogStrong();
        });
        findComponentById(ResourceTable.Id_button5).setClickedListener(component -> {
            tipsDialog();
        });
        findComponentById(ResourceTable.Id_button6).setClickedListener(component -> {
            loadingDialog();
        });
        findComponentById(ResourceTable.Id_button7).setClickedListener(component -> {
            progressDialog();
        });
        findComponentById(ResourceTable.Id_button8).setClickedListener(component -> {
            dialogView();
        });
        findComponentById(ResourceTable.Id_button9).setClickedListener(component -> {
            homeBannerDialog();
        });
        findComponentById(ResourceTable.Id_button10).setClickedListener(component -> {
            evaluateDialog();
        });
        findComponentById(ResourceTable.Id_button11).setClickedListener(component -> {
            updateHead();
        });
        findComponentById(ResourceTable.Id_button12).setClickedListener(component -> {
            listDialog();
        });
        findComponentById(ResourceTable.Id_button13).setClickedListener(component -> {
            bottomListDialog();
        });
        findComponentById(ResourceTable.Id_button14).setClickedListener(component -> {
            shareDialog();
        });
        findComponentById(ResourceTable.Id_text).setClickedListener(component -> {
            updateHead();
        });

    }

    private void shareDialog() {
        new TListDialog.Builder(getContext())
                .setListLayoutRes(ResourceTable.Layout_dialog_share_recycler, Component.HORIZONTAL)
                .setScreenWidthAspect(getAbility(), 1.0f)
                .setHeight(600)
                .setGravity(LayoutAlignment.BOTTOM)
                .setAdapter(new TBaseAdapter<String>(getContext(),ResourceTable.Layout_item_share, Arrays.asList(sharePlatform)) {
                    @Override
                    protected void onBind(BindViewHolder holder, int position, String s) {

                        holder.setText(ResourceTable.Id_tv, s);
                    }
                })
                .setOnAdapterItemClickListener(new TBaseAdapter.OnAdapterItemClickListener<String>() {
                    @Override
                    public void onItemClick(BindViewHolder holder, int position, String item, TDialog tDialog) {
                        new ToastDialog(getContext())
                                .setText(item)
                                .show();
                        tDialog.destroy();
                    }
                })
                .create()
                .show();
    }

    private void bottomListDialog() {

        TListDialog tListDialog = new TListDialog.Builder(getContext())
                .setScreenHeightAspect(getAbility(), 0.5f)
                .setScreenWidthAspect(getAbility(), 1.0f)
                .setGravity(LayoutAlignment.BOTTOM)
                .setAdapter(new TBaseAdapter<String>(getContext(),ResourceTable.Layout_item_simple_text, Arrays.asList(data)) {
                    @Override
                    protected void onBind(BindViewHolder holder, int position, String s) {
                        holder.setText(ResourceTable.Id_tv, s);
                    }
                })
                .setOnAdapterItemClickListener(new TBaseAdapter.OnAdapterItemClickListener<String>() {
                    @Override
                    public void onItemClick(BindViewHolder holder, int position, String s, TDialog tDialog) {
                        new ToastDialog(getContext())
                                .setText("click:" + s )
                                .show();
                        tDialog.destroy();
                    }
                })
                .setOnDismissListener(new CommonDialog.DestroyedListener() {
                    @Override
                    public void onDestroy() {
                        new ToastDialog(getContext())
                                .setText("setOnDismissListener 回调")
                                .show();
                    }
                })
                .create();
        tListDialog.setOffset(0,100);
        tListDialog.show();


    }

    private void listDialog() {
        new TListDialog.Builder(getContext())
                .setHeight(600)
                .setScreenWidthAspect(getAbility(), 0.8f)
                .setGravity(LayoutAlignment.CENTER)
                .setAdapter(new TBaseAdapter<String>(getContext(),ResourceTable.Layout_item_simple_text, Arrays.asList(data)) {

                    @Override
                    protected void onBind(BindViewHolder holder, int position, String s) {
                        holder.setText(ResourceTable.Id_tv, s);
                    }
                })
                .setOnAdapterItemClickListener(new TBaseAdapter.OnAdapterItemClickListener<String>() {
                    @Override
                    public void onItemClick(BindViewHolder holder, int position, String s, TDialog tDialog) {
                        new ToastDialog(getContext())
                                .setText("click:" + s )
                                .show();
                        tDialog.destroy();
                    }
                })
                .create()
                .show();
    }

    private void updateHead() {
        new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_change_avatar)
                .setScreenWidthAspect(this, 1.0f)
                .setHeight(650)
                .setGravity(LayoutAlignment.BOTTOM)
                .addOnClickListener(ResourceTable.Id_tv_open_camera, ResourceTable.Id_tv_open_album, ResourceTable.Id_tv_cancel)
                .setOnViewClickListener(new OnViewClickListener() {
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, Component view, TDialog tDialog) {
                        switch (view.getId()) {
                            case ResourceTable.Id_tv_open_camera:
                                new ToastDialog(getContext())
                                        .setText("打开相机:" )
                                        .show();
                                tDialog.destroy();
                                break;
                            case ResourceTable.Id_tv_open_album:
                                new ToastDialog(getContext())
                                        .setText("打开相册:" )
                                        .show();
                                tDialog.destroy();
                                break;
                            case ResourceTable.Id_tv_cancel:
                                tDialog.destroy();
                                break;
                        }
                    }
                })
                .create()
                .show();
    }

    private void evaluateDialog() {


        TDialog tDialog = new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_evaluate)
                .setScreenWidthAspect(this, 1.0f)
                .setGravity(LayoutAlignment.CENTER)
                .addOnClickListener(ResourceTable.Id_btn_evluate)
                .setTextField(ResourceTable.Id_editText)
                .setOnBindViewListener(viewHolder -> {
                    final TextField editText = viewHolder.getView(ResourceTable.Id_editText);
                    handler.postTask(() -> showKeyboard(editText),500);
                })
                .setOnViewClickListener(new OnViewClickListener() {
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, Component view, TDialog tDialog) {
                        TextField editText = viewHolder.getView(ResourceTable.Id_editText);
                        String content = editText.getText().trim();
                        new ToastDialog(getContext())
                                .setText("评价内容:" + content)
                                .show();
                        tDialog.destroy();
                    }
                })
                .create();
        tDialog.show();

    }

    private void homeBannerDialog() {
        TDialog tDialog = new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_home_ad)
                .setScreenHeightAspect(this, 0.7f)
                .setScreenWidthAspect(this, 0.8f)
                .setOnBindViewListener(new OnBindViewListener() {
                    @Override
                    public void bindView(BindViewHolder viewHolder) {
                        //可对图片进行修改
                    }
                })
                .addOnClickListener(ResourceTable.Id_iv_close)
                .setOnViewClickListener(new OnViewClickListener() {
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, Component view, TDialog tDialog) {
                        tDialog.destroy();
                    }
                })
                .create();
        tDialog.setTransparent(true);
        tDialog.show();


    }

    private void dialogView() {
        Text textView = new Text(this);
        textView.setText("DialogView");
        textView.setTextSize(25);
        textView.setTextColor(new Color(getContext().getColor(ResourceTable.Color_holo_red_dark)));
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(fromArgbInt(getContext().getColor(ResourceTable.Color_holo_green_dark)));
        textView.setBackground(shapeElement);
        textView.setTextSize(100);
        tDialog = new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_loading)
                .setDialogView(textView)
                .setHeight(400)
                .setWidth(600)
                .setCancelableOutside(true)
                .create();
        tDialog.show();

    }

    private void progressDialog() {
        tDialog = new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_loading_progress)
                .setScreenWidthAspect(this, 0.8f)
                .setCancelableOutside(true)
                .setOnBindViewListener(new OnBindViewListener() {
                    @Override
                    public void bindView(BindViewHolder viewHolder) {
                        progressBar = viewHolder.getView(ResourceTable.Id_progress_bar);
                        tvProgress = viewHolder.getView(ResourceTable.Id_tv_progress);
                    }
                })
                .setOnDismissListener(new CommonDialog.DestroyedListener() {
                    @Override
                    public void onDestroy() {
                        handler.removeEvent(WHAT_PROGRESS);
                        currProgress = 5;
                    }

                })
                .create();
        tDialog.show();
        handler.sendEvent(WHAT_PROGRESS, 1000);
    }

    private void loadingDialog() {
        tDialog = new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_loading)
                .setHeight(300)
                .setWidth(300)
                .setCancelableOutside(false)
                .create();
        tDialog.show();

        handler.sendEvent(WHAT_LOADING, 5 * 1000);
    }

    private void tipsDialog() {
        new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_vb_convert)
                .setScreenWidthAspect(this, 0.85f)
                .setCancelableOutside(false)
                .addOnClickListener(ResourceTable.Id_tv_jiuyuan_desc, ResourceTable.Id_tv_cancel, ResourceTable.Id_tv_confirm)
                .setOnViewClickListener(new OnViewClickListener() {
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, Component view, TDialog tDialog) {
                        switch (view.getId()) {
                            case ResourceTable.Id_tv_jiuyuan_desc:
                                new ToastDialog(getContext())
                                        .setText("进入说明界面")
                                        .show();
                                break;
                            case ResourceTable.Id_tv_cancel:
                                tDialog.destroy();
                                break;
                            case ResourceTable.Id_tv_confirm:
                                new ToastDialog(getContext())
                                        .setText("执行优惠券兑换逻辑")
                                        .show();
                                tDialog.destroy();
                                break;
                        }
                    }
                })
                .create()
                .show();
    }

    private void upgradeDialogStrong() {
        new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_version_upgrde_strong)
                .setScreenWidthAspect(this, 0.7f)
                .addOnClickListener(ResourceTable.Id_tv_confirm)
                .setCancelableOutside(false)
                .setOnKeyListener(new IDialog.KeyboardCallback() {
                    @Override
                    public boolean clickKey(IDialog iDialog, KeyEvent keyEvent) {
                        if (keyEvent.getKeyCode() == KeyEvent.KEY_BACK) {
                            new ToastDialog(getContext())
                                    .setText("返回健无效，请强制升级后使用")
                                    .show();
                            return true;
                        }
                        return false;
                    }

                })
                .setOnViewClickListener(new OnViewClickListener() {
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, Component view, TDialog tDialog) {
                        new ToastDialog(getContext())
                                .setText("开始下载新版本apk文件")
                                .show();
                    }
                })
                .create()
                .show();
    }

    private void upgradeDialog() {

        new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_version_upgrde)
                .setScreenWidthAspect(this, 0.7f)
                .addOnClickListener(ResourceTable.Id_tv_cancel, ResourceTable.Id_tv_confirm)
                .setOnViewClickListener(new OnViewClickListener() {
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, Component view, TDialog tDialog) {
                        switch (view.getId()) {
                            case ResourceTable.Id_tv_cancel:
                                tDialog.destroy();
                                break;
                            case ResourceTable.Id_tv_confirm:
                                new ToastDialog(getContext())
                                        .setText("开始下载新版本apk文件")
                                        .show();
                                tDialog.destroy();
                                break;
                        }
                    }
                })
                .create()
                .show();
    }

    private void useTDialog() {
        TDialog tDialog = new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_click)    //设置弹窗展示的xml布局
                .setWidth(600)  //设置弹窗宽度(px)
                .setHeight(800)  //设置弹窗高度(px)
                .setScreenWidthAspect(this, 0.8f)   //设置弹窗宽度(参数aspect为屏幕宽度比例 0 - 1f)
                .setScreenHeightAspect(this, 0.3f)  //设置弹窗高度(参数aspect为屏幕宽度比例 0 - 1f)
                .setGravity(LayoutAlignment.CENTER)     //设置弹窗展示位置
                .setTag("DialogTest")   //设置Tag
                .setDimAmount(0.6f)     //设置弹窗背景透明度(0-1f)
                .setCancelableOutside(true)     //弹窗在界面外是否可以点击取消
                .setOnDismissListener(new CommonDialog.DestroyedListener() {
                    //弹窗隐藏时回调方法
                    @Override
                    public void onDestroy() {
                        new ToastDialog(getContext())
                                .setText("弹窗消失回调")
                                .show();
                    }

                })
                .setOnBindViewListener(new OnBindViewListener() {   //通过BindViewHolder拿到控件对象,进行修改
                    @Override
                    public void bindView(BindViewHolder bindViewHolder) {
                        bindViewHolder.setText(ResourceTable.Id_tv_content, "abcdef");
                        bindViewHolder.setText(ResourceTable.Id_tv_title, "我是Title");
                    }
                })
                .addOnClickListener(ResourceTable.Id_btn_left, ResourceTable.Id_btn_right, ResourceTable.Id_tv_title)   //添加进行点击控件的id
                .setOnViewClickListener(new OnViewClickListener() {
                    //View控件点击事件回调
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, Component view, TDialog tDialog) {
                        switch (view.getId()) {
                            case ResourceTable.Id_btn_left:
                                new ToastDialog(getContext())
                                        .setText("left clicked")
                                        .show();
                                break;
                            case ResourceTable.Id_btn_right:
                                new ToastDialog(getContext())
                                        .setText("right clicked")
                                        .show();
                                tDialog.remove();
                                break;
                            case ResourceTable.Id_tv_title:
                                new ToastDialog(getContext())
                                        .setText("title clicked")
                                        .show();
                                viewHolder.setText(ResourceTable.Id_tv_title, "Title点击了");
                                break;
                        }
                    }
                })
                .setOnKeyListener(new IDialog.KeyboardCallback() {
                    @Override
                    public boolean clickKey(IDialog iDialog, KeyEvent keyEvent) {
                        new ToastDialog(getContext())
                                .setText("按键 keyCode:"+keyEvent.getKeyCode())
                                .show();
                        return false;
                    }
                })
                .create();//创建TDialog
        tDialog.show();
    }

    class MyEventHandler extends EventHandler{

        public MyEventHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case WHAT_LOADING:
                    if (tDialog != null) {
                        tDialog.destroy();
                    }
                    return;

                case WHAT_PROGRESS:
                    currProgress += 5;
                    progressBar.setProgressValue(currProgress);
                    tvProgress.setText("progress:" + currProgress + "/100");
                    if (tDialog != null && currProgress >= 100) {
                        currProgress = 0;
                        tDialog.destroy();
                        tDialog = null;
                    } else {
                        handler.sendEvent(WHAT_PROGRESS, 1000);
                    }
                    return;
            }
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    public static void showKeyboard(final Component view) {
        view.requestFocus();
        try {
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method startInput = inputClass.getMethod("startInput",int.class,boolean.class);
            startInput.invoke(object,1,true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}

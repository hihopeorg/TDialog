package com.timmy.tdialog;

import com.timmy.tdialog.base.BindViewHolder;
import com.timmy.tdialog.base.TBaseAdapter;
import com.timmy.tdialog.base.TController;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.*;

public class TControllerTest {
    public static Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

    @Test
    public void getTextField() {
        TController tController = new TController();
        assertEquals(0,tController.getTextField());
    }

    @Test
    public void setTextField() {
        TController tController = new TController();
        tController.setTextField(789);
        assertEquals(789,tController.getTextField());
    }

    @Test
    public void getLayoutRes() {
        TController tController = new TController();
        assertEquals(0,tController.getLayoutRes());
    }

    @Test
    public void setLayoutRes() {
        TController tController = new TController();
        tController.setLayoutRes(789);
        assertEquals(789,tController.getLayoutRes());
    }

    @Test
    public void getHeight() {
        TController tController = new TController();
        assertEquals(0,tController.getHeight());
    }

    @Test
    public void getWidth() {
        TController tController = new TController();
        assertEquals(0,tController.getWidth());
    }

    @Test
    public void setWidth() {
        TController tController = new TController();
        tController.setWidth(879);
        assertEquals(879,tController.getWidth());
    }

    @Test
    public void getDimAmount() {
        TController tController = new TController();
        assertEquals(0,tController.getDimAmount(),0.0);
    }

    @Test
    public void getGravity() {
        TController tController = new TController();
        assertEquals(0,tController.getGravity());
    }

    @Test
    public void getTag() {
        TController tController = new TController();
        assertEquals(null,tController.getTag());
    }

    @Test
    public void getIds() {
        TController tController = new TController();
        assertEquals(null,tController.getIds());
    }

    @Test
    public void isCancelableOutside() {
        TController tController = new TController();
        assertEquals(false,tController.isCancelableOutside());
    }

    @Test
    public void getOnViewClickListener() {
        TController tController = new TController();
        assertEquals(null,tController.getOnViewClickListener());
    }

    @Test
    public void getOrientation() {
        TController tController = new TController();
        assertEquals(0,tController.getOrientation());
    }

    @Test
    public void getOnBindViewListener() {
        TController tController = new TController();
        assertEquals(null,tController.getOnBindViewListener());
    }

    @Test
    public void getOnDismissListener() {
        TController tController = new TController();
        assertEquals(null,tController.getOnDismissListener());
    }

    @Test
    public void getOnKeyListener() {
        TController tController = new TController();
        assertEquals(null,tController.getOnKeyListener());
    }

    @Test
    public void getDialogView() {
        TController tController = new TController();
        assertEquals(null,tController.getDialogView());
    }

    @Test
    public void getAdapter() {
        TController tController = new TController();
        assertEquals(null,tController.getAdapter());
    }

    @Test
    public void setAdapter() {
        TController tController = new TController();
        TBaseAdapter tBaseAdapter = new TBaseAdapter(context,798,null) {
            @Override
            protected void onBind(BindViewHolder holder, int position, Object o) {

            }
        };
        tController.setAdapter(tBaseAdapter);
        assertEquals(tBaseAdapter,tController.getAdapter());
    }

    @Test
    public void getAdapterItemClickListener() {
        TController tController = new TController();
        assertEquals(null,tController.getAdapterItemClickListener());
    }

    @Test
    public void setAdapterItemClickListener() {
        TController tController = new TController();
        TBaseAdapter.OnAdapterItemClickListener onAdapterItemClickListener = new TBaseAdapter.OnAdapterItemClickListener() {
            @Override
            public void onItemClick(BindViewHolder holder, int position, Object o, TDialog tDialog) {

            }
        };
        tController.setAdapterItemClickListener(onAdapterItemClickListener);
        assertEquals(onAdapterItemClickListener,tController.getAdapterItemClickListener());
    }

    @Test
    public void getDialogAnimationRes() {
        TController tController = new TController();
        assertEquals(0,tController.getDialogAnimationRes());
    }
}
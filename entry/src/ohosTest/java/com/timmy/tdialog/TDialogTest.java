package com.timmy.tdialog;

import org.junit.Test;

import static com.timmy.tdialog.TControllerTest.context;
import static org.junit.Assert.*;

public class TDialogTest {

    @Test
    public void getTextFieldId() {
        TDialog tDialog = new TDialog(context);
        assertEquals(0,tDialog.getTextFieldId());
    }

    @Test
    public void getGravity() {
        TDialog tDialog = new TDialog(context);
        assertEquals(0,tDialog.getGravity());
    }

    @Test
    public void getDimAmount() {
        TDialog tDialog = new TDialog(context);
        assertEquals(0,tDialog.getDimAmount(),0.0);
    }

    @Test
    public void getDialogHeight() {
        TDialog tDialog = new TDialog(context);
        assertEquals(0,tDialog.getDialogHeight());
    }

    @Test
    public void getDialogWidth() {
        TDialog tDialog = new TDialog(context);
        assertEquals(0,tDialog.getDialogWidth());
    }

    @Test
    public void getFragmentTag() {
        TDialog tDialog = new TDialog(context);
        assertEquals(null,tDialog.getFragmentTag());
    }

    @Test
    public void getOnViewClickListener() {
        TDialog tDialog = new TDialog(context);
        assertEquals(null,tDialog.getOnViewClickListener());
    }

    @Test
    public void isCancelableOutside() {
        TDialog tDialog = new TDialog(context);
        assertEquals(false,tDialog.isCancelableOutside());
    }

    @Test
    public void getDialogAnimationRes() {
        TDialog tDialog = new TDialog(context);
        assertEquals(0,tDialog.getDialogAnimationRes());
    }

    @Test
    public void getOnKeyListener() {
        TDialog tDialog = new TDialog(context);
        assertEquals(null,tDialog.getOnKeyListener());
    }
}
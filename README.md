# TDialog

**本项目是基于开源项目 TDialog 进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/Timmy-zzh/TDialog ）追踪到原项目版本**

#### 项目介绍

- 项目名称：TDialog
- 所属系列：ohos的第三方组件适配移植
- 功能：提供封装好的dialog框架使用工具库
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 调用差异：无
- 原项目Doc地址：https://github.com/Timmy-zzh/TDialog
- 无原基线版本
- 编程语言：Java 

#### 安装教程
方法1.
1. 下载本三方库的har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在entry级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

方法2.
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.timmy.tdialog.ohos:tdialog:1.0.0'
}
```
#### 效果演示
<img src="gif/演示效果.gif"/>

#### 使用说明

1.dialog的基本使用

```
        TDialog tDialog = new TDialog.Builder(getContext())
                .setLayoutRes(ResourceTable.Layout_dialog_click)    //设置弹窗展示的xml布局
                .setWidth(600)  //设置弹窗宽度(px)
                .setHeight(800)  //设置弹窗高度(px)
                .setScreenWidthAspect(this, 0.8f)   //设置弹窗宽度(参数aspect为屏幕宽度比例 0 - 1f)
                .setScreenHeightAspect(this, 0.3f)  //设置弹窗高度(参数aspect为屏幕宽度比例 0 - 1f)
                .setGravity(LayoutAlignment.CENTER)     //设置弹窗展示位置
                .setTag("DialogTest")   //设置Tag
                .setDimAmount(0.6f)     //设置弹窗背景透明度(0-1f)
                .setCancelableOutside(true)     //弹窗在界面外是否可以点击取消
                .setOnDismissListener(new CommonDialog.DestroyedListener() {
                    //弹窗隐藏时回调方法
                    @Override
                    public void onDestroy() {
                        new ToastDialog(getContext())
                                .setText("弹窗消失回调")
                                .show();
                    }

                })
                .setOnBindViewListener(new OnBindViewListener() {   //通过BindViewHolder拿到控件对象,进行修改
                    @Override
                    public void bindView(BindViewHolder bindViewHolder) {
                        bindViewHolder.setText(ResourceTable.Id_tv_content, "abcdef");
                        bindViewHolder.setText(ResourceTable.Id_tv_title, "我是Title");
                    }
                })
                .addOnClickListener(ResourceTable.Id_btn_left, ResourceTable.Id_btn_right, ResourceTable.Id_tv_title)   //添加进行点击控件的id
                .setOnViewClickListener(new OnViewClickListener() {
                    //View控件点击事件回调
                    @Override
                    public void onViewClick(BindViewHolder viewHolder, Component view, TDialog tDialog) {
                        switch (view.getId()) {
                            case ResourceTable.Id_btn_left:
                                new ToastDialog(getContext())
                                        .setText("left clicked")
                                        .show();
                                break;
                            case ResourceTable.Id_btn_right:
                                new ToastDialog(getContext())
                                        .setText("right clicked")
                                        .show();
                                tDialog.remove();
                                break;
                            case ResourceTable.Id_tv_title:
                                new ToastDialog(getContext())
                                        .setText("title clicked")
                                        .show();
                                viewHolder.setText(ResourceTable.Id_tv_title, "Title点击了");
                                break;
                        }
                    }
                })
                .setOnKeyListener(new IDialog.KeyboardCallback() {
                    @Override
                    public boolean clickKey(IDialog iDialog, KeyEvent keyEvent) {
                        new ToastDialog(getContext())
                                .setText("按键 keyCode:"+keyEvent.getKeyCode())
                                .show();
                        return false;
                    }
                })
                .create();//创建TDialog
        tDialog.show();
```


#### 版本迭代

- v1.0.0

    -提供封装好的dialog框架使用工具库
	



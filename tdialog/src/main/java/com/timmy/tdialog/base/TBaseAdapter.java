package com.timmy.tdialog.base;


import com.timmy.tdialog.TDialog;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.List;

/**
 *
 * @author Timmy
 * @time 2018/1/24 14:39
 * @GitHub https://github.com/Timmy-zzh/TDialog
 **/
public abstract class TBaseAdapter<T> extends BaseItemProvider {

    private final int layoutRes;
    private List<T> datas;
    private OnAdapterItemClickListener adapterItemClickListener;
    private TDialog dialog;
    private Context context;

    protected abstract void onBind(BindViewHolder holder, int position, T t);

    public TBaseAdapter(Context context,int layoutRes, List<T> datas) {
        this.context = context;
        this.layoutRes = layoutRes;
        this.datas = datas;

    }



    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        BindViewHolder holder = new BindViewHolder(LayoutScatter.getInstance(context).parse(layoutRes, componentContainer, false));
        onBind(holder, i, datas.get(i));
        holder.bindView.setClickedListener(new Component.ClickedListener () {
            @Override
            public void onClick(Component component) {
                adapterItemClickListener.onItemClick(holder, i, datas.get(i), dialog);
            }
        });
        return holder.bindView;
    }





    public void setTDialog(TDialog tDialog) {
        this.dialog = tDialog;
    }

    public interface OnAdapterItemClickListener<T> {
        void onItemClick(BindViewHolder holder, int position, T t, TDialog tDialog);
    }

    public void setOnAdapterItemClickListener(OnAdapterItemClickListener listener) {
        this.adapterItemClickListener = listener;
    }
}

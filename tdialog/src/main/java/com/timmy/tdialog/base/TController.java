package com.timmy.tdialog.base;

import com.timmy.tdialog.ResourceTable;
import com.timmy.tdialog.listener.OnBindViewListener;
import com.timmy.tdialog.listener.OnViewClickListener;
import ohos.agp.components.Component;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import java.io.Serializable;

/**
 * 数据保存封装的容器类
 *
 * @author Timmy
 * @time 2018/1/24 14:40
 * @GitHub https://github.com/Timmy-zzh/TDialog
 **/
public class TController<A extends TBaseAdapter> implements Sequenceable, Serializable {

    private int layoutRes;
    private int height;
    private int width;
    private float dimAmount;
    private int gravity;
    private String tag;
    private int[] ids;
    private boolean isCancelableOutside;
    private OnViewClickListener onViewClickListener;
    private OnBindViewListener onBindViewListener;
    private A adapter;
    private TBaseAdapter.OnAdapterItemClickListener adapterItemClickListener;
    private int orientation;
    private int dialogAnimationRes;
    private Component dialogView;
    private CommonDialog.DestroyedListener onDismissListener;
    private IDialog.KeyboardCallback onKeyListener;

    public int getTextField() {
        return textField;
    }

    public void setTextField(int textField) {
        this.textField = textField;
    }

    private int textField;

    //////////////////////////////////////////Parcelable持久化//////////////////////////////////////////////////////
    public TController() {
    }

    protected TController(Parcel in) {
        layoutRes = in.readInt();
        height = in.readInt();
        width = in.readInt();
        dimAmount = in.readFloat();
        gravity = in.readInt();
        tag = in.readString();
        ids = in.readIntArray();
        isCancelableOutside = in.readByte() != 0;
        orientation = in.readInt();
    }

    public static final Producer<TController> CREATOR = new Producer<TController>() {
        @Override
        public TController createFromParcel(Parcel in) {
            return new TController(in);
        }
    };


    ////////////////////////////////////////////////////////////////////////////////////////////////



    public int getLayoutRes() {
        return layoutRes;
    }

    public void setLayoutRes(int layoutRes) {
        this.layoutRes = layoutRes;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int mWidth) {
        this.width = mWidth;
    }

    public float getDimAmount() {
        return dimAmount;
    }

    public int getGravity() {
        return gravity;
    }

    public String getTag() {
        return tag;
    }

    public int[] getIds() {
        return ids;
    }

    public boolean isCancelableOutside() {
        return isCancelableOutside;
    }

    public OnViewClickListener getOnViewClickListener() {
        return onViewClickListener;
    }

    public int getOrientation() {
        return orientation;
    }

    public OnBindViewListener getOnBindViewListener() {
        return onBindViewListener;
    }

    public CommonDialog.DestroyedListener getOnDismissListener() {
        return onDismissListener;
    }

    public IDialog.KeyboardCallback getOnKeyListener() {
        return onKeyListener;
    }

    public Component getDialogView() {
        return dialogView;
    }

    //列表
    public A getAdapter() {
        return adapter;
    }

    public void setAdapter(A adapter) {
        this.adapter = adapter;
    }

    public TBaseAdapter.OnAdapterItemClickListener getAdapterItemClickListener() {
        return adapterItemClickListener;
    }

    public void setAdapterItemClickListener(TBaseAdapter.OnAdapterItemClickListener adapterItemClickListener) {
        this.adapterItemClickListener = adapterItemClickListener;
    }

    public int getDialogAnimationRes() {
        return dialogAnimationRes;
    }

    @Override
    public boolean marshalling(Parcel dest) {
        dest.writeInt(layoutRes);
        dest.writeInt(height);
        dest.writeInt(width);
        dest.writeFloat(dimAmount);
        dest.writeInt(gravity);
        dest.writeString(tag);
        dest.writeIntArray(ids);
        dest.writeByte((byte) (isCancelableOutside ? 1 : 0));
        dest.writeInt(orientation);
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }

    /**************************************************************************
     */
    public static class TParams<A extends TBaseAdapter> {

        public int mLayoutRes;
        public int mWidth;
        public int mHeight;
        public float mDimAmount = 0.2f;
        public int mGravity = LayoutAlignment.CENTER;
        public String mTag = "TDialog";
        public int[] ids;
        public boolean mIsCancelableOutside = true;
        public OnViewClickListener mOnViewClickListener;
        public OnBindViewListener bindViewListener;
        public int mDialogAnimationRes = 0;//弹窗动画
        //列表
        public A adapter;
        public TBaseAdapter.OnAdapterItemClickListener adapterItemClickListener;
        public int listLayoutRes;
        public int orientation = Component.VERTICAL;//默认RecyclerView的列表方向为垂直方向
        public Component mDialogView;//直接使用传入进来的View,而不需要通过解析Xml
        public CommonDialog.DestroyedListener mOnDismissListener;
        public IDialog.KeyboardCallback mKeyListener;

        public int textField;

        public void apply(TController tController) {
            if (mLayoutRes > 0) {
                tController.layoutRes = mLayoutRes;
            }
            if (mDialogView != null) {
                tController.dialogView = mDialogView;
            }
            if (mWidth > 0) {
                tController.width = mWidth;
            }
            if (mHeight > 0) {
                tController.height = mHeight;
            }
            tController.dimAmount = mDimAmount;
            tController.gravity = mGravity;
            tController.tag = mTag;
            if (ids != null) {
                tController.ids = ids;
            }
            tController.isCancelableOutside = mIsCancelableOutside;
            tController.onViewClickListener = mOnViewClickListener;
            tController.onBindViewListener = bindViewListener;
            tController.onDismissListener = mOnDismissListener;
            tController.dialogAnimationRes = mDialogAnimationRes;
            tController.onKeyListener =mKeyListener;

            if (adapter != null) {
                tController.setAdapter(adapter);
                if (listLayoutRes <= 0) {//使用默认的布局
                    tController.setLayoutRes(ResourceTable.Layout_dialog_recycler);
                } else {
                    tController.setLayoutRes(listLayoutRes);
                }
                tController.orientation = orientation;
            } else {
                if (tController.getLayoutRes() <= 0 && tController.getDialogView() == null) {
                    throw new IllegalArgumentException("请先调用setLayoutRes()方法设置弹窗所需的xml布局!");
                }
            }
            if (adapterItemClickListener != null) {
                tController.setAdapterItemClickListener(adapterItemClickListener);
            }

            //如果宽高都没有设置,则默认给弹窗提供宽度为600
            if (tController.width <= 0 && tController.height <= 0) {
                tController.width = 600;
            }

            if(textField != 0){
                tController.textField = textField;
            }
        }
    }
}

package com.timmy.tdialog.base;


import com.timmy.tdialog.TDialog;
import ohos.agp.components.AbsButton;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.PixelMap;
import ohos.utils.PlainArray;

import java.io.IOException;

import static ohos.agp.colors.RgbColor.fromArgbInt;

/**
 * 借鉴RecyclerView.Adapter的ViewHolder写法
 * 将Dialog的根布局传入,主要处理点击方法
 *
 * @author Timmy
 * @time 2017/12/28 16:18
 **/
public class BindViewHolder  {

    public Component bindView;
    private PlainArray<Component> views;
    private TDialog dialog;

    public BindViewHolder(final Component view) {
        this.bindView = view;
        this.views = new PlainArray<>();
    }

    public BindViewHolder(Component view, TDialog dialog) {
        this.bindView = view;
        this.dialog = dialog;
        views = new PlainArray<>();
    }

    public <T extends Component> T getView( int viewId) {

//        Component view = null;
//        if (views.get(viewId).get() == null) {
//            view = bindView.findComponentById(viewId);
//            views.put(viewId, view);
//        }

        return (T) bindView.findComponentById(viewId);
    }

    public BindViewHolder addOnClickListener(final int viewId) {
        final Component view = getView(viewId);
        if (view != null) {
            if (!view.isClickable()) {
                view.setClickable(true);
            }
            view.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (dialog.getOnViewClickListener() != null) {
                        dialog.getOnViewClickListener().onViewClick(BindViewHolder.this,view, dialog);
                    }
                }
            });
        }
        return this;
    }

    public BindViewHolder setText(int viewId, CharSequence value) {
        Text view = getView(viewId);
        view.setText((String) value);
        return this;
    }

    public BindViewHolder setText(int viewId, int strId) {
        Text view = getView(viewId);
        view.setText(strId);
        return this;
    }

    /**
     * Will set the image of an ImageView from a resource id.
     *
     * @param viewId     The view id.
     * @param imageResId The image resource id.
     * @return The BaseViewHolder for chaining.
     */
    public BindViewHolder setImageResource(int viewId,int imageResId) {
        Image view = getView(viewId);
        try {
            Resource resource = view.getResourceManager().getResource(imageResId);
            view.setImageElement(new PixelMapElement(resource));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * Will set background color of a view.
     *
     * @param viewId The view id.
     * @param color  A color, not a resource id.
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setBackgroundColor(int viewId, int color) {
        Component view = getView(viewId);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(fromArgbInt(color));
        view.setBackground(shapeElement);
        return this;
    }
    /**
     * Will set background of a view.
     *
     * @param viewId        The view id.
     * @param backgroundRes A resource to use as a background.
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setBackgroundRes(int viewId, int backgroundRes) {
        Component view = getView(viewId);
        try {
            Resource resource = view.getResourceManager().getResource(backgroundRes);
            view.setBackground(new PixelMapElement(resource));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * Will set text color of a TextView.
     *
     * @param viewId    The view id.
     * @param textColor The text color (not a resource id).
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setTextColor(int viewId, int textColor) {
        Text view = getView(viewId);
        view.setTextColor(new Color(textColor));
        return this;
    }


    /**
     * Will set the image of an ImageView from a drawable.
     *
     * @param viewId   The view id.
     * @param drawable The image drawable.
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setImageDrawable(int viewId, Element drawable) {
        Image view = getView(viewId);
        view.setImageElement(drawable);
        return this;
    }

    /**
     * Add an action to set the image of an image view. Can be called multiple times.
     */
    public BindViewHolder setImageBitmap(int viewId, PixelMap bitmap) {
        Image view = getView(viewId);
        view.setPixelMap(bitmap);
        return this;
    }

    /**
     * Add an action to set the alpha of a view. Can be called multiple times.
     * Alpha between 0-1.
     */
    public BindViewHolder setAlpha(int viewId, float value) {
        getView(viewId).setAlpha(value);
        return this;
    }

    /**
     * Set a view visibility to VISIBLE (true) or GONE (false).
     *
     * @param viewId  The view id.
     * @param visible True for VISIBLE, false for GONE.
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setGone(int viewId, boolean visible) {
        Component view = getView(viewId);
        view.setVisibility(visible ? Component.VISIBLE : Component.HIDE);
        return this;
    }

    /**
     * Set a view visibility to VISIBLE (true) or INVISIBLE (false).
     *
     * @param viewId  The view id.
     * @param visible True for VISIBLE, false for INVISIBLE.
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setVisible(int viewId, boolean visible) {
        Component view = getView(viewId);
        view.setVisibility(visible ? Component.VISIBLE : Component.INVISIBLE);
        return this;
    }

    public BindViewHolder setVisibility(int viewId, int visible) {
        getView(viewId).setVisibility(visible);
        return this;
    }



    /**
     * Sets the progress of a ProgressBar.
     *
     * @param viewId   The view id.
     * @param progress The progress.
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setProgress( int viewId, int progress) {
        ProgressBar view = getView(viewId);
        view.setProgressValue(progress);
        return this;
    }

    /**
     * Sets the progress and max of a ProgressBar.
     *
     * @param viewId   The view id.
     * @param progress The progress.
     * @param max      The max value of a ProgressBar.
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setProgress(int viewId, int progress, int max) {
        ProgressBar view = getView(viewId);
        view.setMaxValue(max);
        view.setProgressValue(progress);
        return this;
    }

    /**
     * Sets the range of a ProgressBar to 0...max.
     *
     * @param viewId The view id.
     * @param max    The max value of a ProgressBar.
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setMax(int viewId, int max) {
        ProgressBar view = getView(viewId);
        view.setMaxValue(max);
        return this;
    }


    /**
     * Sets the on click listener of the view.
     *
     * @param viewId   The view id.
     * @param listener The on click listener;
     * @return The BindViewHolder for chaining.
     */
    @Deprecated
    public BindViewHolder setOnClickListener(int viewId, Component.ClickedListener listener) {
        Component view = getView(viewId);
        view.setClickedListener(listener);
        return this;
    }

    /**
     * Sets the on touch listener of the view.
     *
     * @param viewId   The view id.
     * @param listener The on touch listener;
     * @return The BindViewHolder for chaining.
     */
    @Deprecated
    public BindViewHolder setOnTouchListener(int viewId, Component.TouchEventListener listener) {
        Component view = getView(viewId);
        view.setTouchEventListener(listener);
        return this;
    }

    /**
     * Sets the on long click listener of the view.
     *
     * @param viewId   The view id.
     * @param listener The on long click listener;
     * @return The BindViewHolder for chaining.
     */
    @Deprecated
    public BindViewHolder setOnLongClickListener(int viewId, Component.LongClickedListener listener) {
        Component view = getView(viewId);
        view.setLongClickedListener(listener);
        return this;
    }


    /**
     * Sets the on checked change listener of the view.
     *
     * @param viewId   The view id.
     * @param listener The checked change listener of compound button.
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setOnCheckedChangeListener(int viewId, AbsButton.CheckedStateChangedListener listener) {
        AbsButton view = getView(viewId);
        view.setCheckedStateChangedListener(listener);
        return this;
    }

    /**
     * Sets the tag of the view.
     *
     * @param viewId The view id.
     * @param tag    The tag;
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setTag(int viewId, Object tag) {
        Component view = getView(viewId);
        view.setTag(tag);
        return this;
    }

    /**
     * Sets the tag of the view.
     *
     * @param viewId The view id.
     * @param key    The key of tag;
     * @param tag    The tag;
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setTag(int viewId, int key, Object tag) {
        Component view = getView(viewId);
        view.setTag(tag);
        return this;
    }

    /**
     * Sets the checked status of a checkable.
     *
     * @param viewId  The view id.
     * @param checked The checked status;
     * @return The BindViewHolder for chaining.
     */
    public BindViewHolder setChecked(int viewId, boolean checked) {
        Component view = getView(viewId);
        // View unable cast to Checkable
        if (view instanceof Checkbox) {
            ((Checkbox) view).setChecked(checked);
        }
        return this;
    }

    /**
     * Sets the adapter of a adapter view.
     *
     * @param viewId  The view id.
     * @param adapter The adapter;
     * @return The BindViewHolder for chaining.
     */
    @SuppressWarnings("unchecked")
    public BindViewHolder setAdapter(int viewId, BaseItemProvider adapter) {
        ListContainer view = getView(viewId);
        view.setItemProvider(adapter);
        return this;
    }
}

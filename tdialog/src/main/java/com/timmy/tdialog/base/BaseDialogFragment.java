package com.timmy.tdialog.base;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.Window;
import ohos.app.Context;

public abstract class BaseDialogFragment extends CommonDialog {

    public static final String TAG = "TDialog";
    private static final float DEFAULT_DIMAMOUNT = 0.2F;
    public Context context;

    public BaseDialogFragment(Context context) {
        super(context);
        this.context = context;
    }

    protected abstract int getLayoutRes();

    protected abstract void bindView(Component view);

    protected abstract Component getDialogView();

    @Override
    protected void create() {
        super.create();
        Component view = null;
        if (getLayoutRes() > 0) {
            view = LayoutScatter.getInstance(context).parse(getLayoutRes(), null, false);
        }
        if (getDialogView() != null) {
            view = getDialogView();
        }


        bindView(view);
        setContentCustomComponent(view);

        this.siteRemovable(true);
        this.setDialogListener(new DialogListener() {
            @Override
            public boolean isTouchOutside() {
                if(isCancelableOutside()){
                    destroy();
                    return true;
                }
                return false;
            }
        });
        if (getOnKeyListener() !=null){
            this.siteKeyboardCallback(getOnKeyListener());
        }
        Window window = getWindow();
        ComponentContainer.LayoutConfig layoutConfig = this.getContentCustomComponent().getLayoutConfig();
        if (window != null) {
            //设置窗体背景色透明
            setTransparent(true);
            //设置宽高
            if (getDialogWidth() > 0) {
                layoutConfig.width = getDialogWidth();
            } else {
                layoutConfig.width = ComponentContainer.LayoutConfig.MATCH_CONTENT;
            }
            if (getDialogHeight() > 0) {
                layoutConfig.height = getDialogHeight();
            } else {
                layoutConfig.height = ComponentContainer.LayoutConfig.MATCH_CONTENT;
            }
            //透明度

            setSize(layoutConfig.width,layoutConfig.height);
        }
        setAlignment(getGravity());
    }




    protected KeyboardCallback getOnKeyListener() {
        return null;
    }

    @Override
    protected void onShow() {
        super.onShow();
    }

    public int getTextFieldId(){return  0;}

    //默认弹窗位置为中心
    public int getGravity() {
        return LayoutAlignment.CENTER;
    }

    //默认宽高为包裹内容
    public int getDialogHeight() {
        return ComponentContainer.LayoutConfig.MATCH_CONTENT;
    }

    public int getDialogWidth() {
        return ComponentContainer.LayoutConfig.MATCH_CONTENT;
    }

    //默认透明度为0.2
    public float getDimAmount() {
        return DEFAULT_DIMAMOUNT;
    }

    public String getFragmentTag() {
        return TAG;
    }


    protected boolean isCancelableOutside() {
        return true;
    }

    //获取弹窗显示动画,子类实现
    protected int getDialogAnimationRes() {
        return 0;
    }

    //获取设备屏幕宽度
    public static final int getScreenWidth(Context context) {
        return DisplayManager.getInstance().getDefaultDisplay(context).get().getRealAttributes().width;
    }

    //获取设备屏幕高度
    public static final int getScreenHeight(Context context) {
        return DisplayManager.getInstance().getDefaultDisplay(context).get().getRealAttributes().height;
    }
}

package com.timmy.tdialog.listener;


import com.timmy.tdialog.TDialog;
import com.timmy.tdialog.base.BindViewHolder;
import ohos.agp.components.Component;

public interface OnViewClickListener {
    void onViewClick(BindViewHolder viewHolder, Component view, TDialog tDialog);
}
